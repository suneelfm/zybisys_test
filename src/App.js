import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AnimListPage from "./pages/animlistPage";
import DetailsPage from "./pages/detailsPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<AnimListPage />} />
        <Route path="/animdetails" element={<DetailsPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
