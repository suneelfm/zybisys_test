export const formatDate = (date) => {
  const d = new Date(date);
  return (
    (d.getDate() + "").padStart(2, "0") +
    " / " +
    (d.getMonth() + 1 + "").padStart(2, "0") +
    " / " +
    d.getFullYear()
  );
};
