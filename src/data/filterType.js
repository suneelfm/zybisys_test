export const filterTypes = [
  "Adventure",
  "Fantasy",
  "Action",
  "Sci-Fi",
  "Comedy",
  "Drama",
  "Mystery",
  "Supernatural",
  "Sports",
  "Romance",
  "Slice of Life",
];
