import { Grid, IconButton, Tooltip } from "@mui/material";
import React from "react";
import styles from "../../../styles/CustomAtoms.module.css";

export default function SearchField({ value, onChange }) {
  return (
    <Grid
      container
      className={styles.searchInputContainer}
      // sx={{ display: { xs: "none", sm: "flex" } }}
    >
      <Grid
        item
        xs={1}
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        <Tooltip title="Search" arrow>
          <IconButton className={styles.searchIconButton}>
            <i className={`fas fa-search ${styles.searchIcon}`} />
          </IconButton>
        </Tooltip>
      </Grid>
      <Grid item xs={10}>
        <input
          className={styles.searchInput}
          placeholder="Search"
          value={value}
          onChange={(e) => onChange(e.target.value)}
        />
      </Grid>
      <Grid
        item
        xs={1}
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        {value !== "" && (
          <Tooltip title="Clear search" arrow>
            <IconButton
              className={`${styles.searchIconButton}`}
              onClick={() => onChange("")}
            >
              <i className={`fas fa-times ${styles.searchIcon}`}></i>
            </IconButton>
          </Tooltip>
        )}
      </Grid>
    </Grid>
  );
}
