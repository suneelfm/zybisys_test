import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Rating,
  Tooltip,
  Typography,
} from "@mui/material";
import React from "react";
import styles from "../../../styles/AnimCard.module.css";

export default function AnimCard({ data, onClick, onDrag }) {
  return (
    <Tooltip title={data.title} arrow followCursor placement="top">
      <Card
        className={styles.cardContainer}
        onClick={onClick}
        draggable
        onDragLeave={(event) => event.preventDefault()}
        onDragOver={(event) => {
          event.preventDefault();
          event.stopPropagation();
        }}
        onDrag={onDrag}
      >
        <CardActionArea className={styles.cardArea}>
          <CardMedia
            component="img"
            height="60%"
            image={data.images.jpg.image_url}
            alt="green iguana"
          />
          <CardContent className={styles.cardContent}>
            <Typography gutterBottom variant="h5" className={styles.cardTitle}>
              {data.title}
            </Typography>
            <Grid container alignItems={"center"}>
              <Rating
                readOnly
                value={data.score / 2}
                precision={0.1}
                className={styles.ratingStarts}
              />
              <Grid
                variant="body2"
                color="text.secondary"
                className={styles.ratingBy}
              >
                {data.scored_by}
              </Grid>
            </Grid>
          </CardContent>
        </CardActionArea>
      </Card>
    </Tooltip>
  );
}
