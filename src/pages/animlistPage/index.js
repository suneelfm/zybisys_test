/* eslint-disable react-hooks/exhaustive-deps */
import { VideoLibrary } from "@material-ui/icons";
import {
  CircularProgress,
  Grid,
  IconButton,
  Menu,
  MenuItem,
  Pagination,
  Tooltip,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import SearchField from "../../components/atoms/searchField/SearchField";
import AnimCard from "../../components/molecules/animCard/AnimCard";
import { filterTypes } from "../../data/filterType";
import { getAnimData } from "../../services/animpage";
import styles from "../../styles/AnimListPage.module.css";

export default function AnimListPage() {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [animData, setAnimData] = useState([]);
  const [pagination, setPagination] = useState({});
  const [pageNo, setPageNo] = useState(1);
  const [searchText, setSearchText] = useState("");
  const [filterItems, setFilterItems] = useState([]);
  const [openWatchList, setopenWatchList] = useState(false);
  const [watchList, setWatchList] = useState(false);
  const [filterMenuEl, setfilterMenuEl] = useState(null);
  const [dragedCard, setdragedCard] = useState({});
  const openFilter = Boolean(filterMenuEl);

  const navigate = useNavigate();

  const getData = async () => {
    const { data, error } = await getAnimData(pageNo);
    if (data) {
      setData(data.data);
      setPagination(data.pagination);
    } else if (error) {
      alert(error);
    }
  };

  useEffect(() => {
    setWatchList(JSON.parse(sessionStorage.getItem("watchList")) || []);
    getData();
  }, [pageNo]);

  useEffect(() => {
    if (filterItems.length > 0) {
      const filtered = [];
      data?.forEach((dt) => {
        const mached = dt.genres?.filter((item) =>
          filterItems.includes(item?.name)
        );
        if (mached.length > 0) {
          filtered.push(dt);
        }
      });
      setFilteredData(filtered);
    } else {
      setFilteredData(data);
    }
  }, [data, filterItems]);

  useEffect(() => {
    const searched = filteredData.filter((item) =>
      item.title.toLocaleLowerCase().includes(searchText.toLocaleLowerCase())
    );
    setAnimData(searched);
  }, [searchText, filteredData]);

  const addToWatchList = () => {
    const existed = watchList.findIndex(
      (item) => item.mal_id === dragedCard.mal_id
    );
    if (existed < 0) {
      sessionStorage.setItem(
        "watchList",
        JSON.stringify([dragedCard, ...watchList])
      );
      setWatchList([dragedCard, ...watchList]);
    }
  };

  const deleteWatchList = (item) => {
    const watchListCopy = [...watchList];
    const index = watchListCopy.findIndex(
      (anime) => anime.mal_id === item.mal_id
    );
    watchListCopy?.splice(index, 1);
    sessionStorage.setItem("watchList", JSON.stringify(watchListCopy));
    setWatchList(watchListCopy);
  };

  return (
    <Grid container className={styles.pageContainer}>
      <Grid container className={styles.header}>
        <Grid item xs={2}></Grid>
        <Grid
          item
          xs={8}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <SearchField
            value={searchText}
            onChange={(value) => setSearchText(value)}
          />
        </Grid>
        <Grid
          item
          xs={2}
          px={1}
          display="flex"
          justifyContent="flex-end"
          alignItems="center"
        >
          <Tooltip title="Filter" arrow>
            <IconButton
              className={styles.iconButton}
              onClick={(e) => setfilterMenuEl(e.currentTarget)}
            >
              <i className="fas fa-filter" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Watchlist" arrow>
            <IconButton
              className={styles.iconButton}
              onClick={() => setopenWatchList(!openWatchList)}
            >
              <VideoLibrary />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      <Grid container className={styles.pageBody}>
        <Grid
          container
          item
          xs={openWatchList ? 9 : 12}
          className={styles.pageCardsDisplay}
        >
          {animData.map((anim, index) => (
            <Grid
              item
              xs={12}
              sm={6}
              md={4}
              lg={3}
              height="calc(30px + 30vmin)"
              p={2}
            >
              <AnimCard
                key={index}
                onDrag={() => setdragedCard(anim)}
                data={anim}
                onClick={() => {
                  sessionStorage.setItem("animDetails", JSON.stringify(anim));
                  navigate("/animdetails");
                }}
              />
            </Grid>
          ))}
        </Grid>
        {openWatchList && (
          <Grid
            item
            xs={openWatchList ? 3 : 0}
            p={1}
            className={styles.watchListContainer}
          >
            <Grid
              p={2}
              textAlign="center"
              className={styles.dropContainer}
              onDragLeave={(event) => event.preventDefault()}
              onDragOver={(event) => {
                event.preventDefault();
                event.stopPropagation();
              }}
              onDrop={addToWatchList}
            >
              <Grid className={styles.dropArea} p={1}>
                Drag and drop your anime here to add to watch list.
              </Grid>
            </Grid>
            <Grid container className={styles.watchListBody}>
              {watchList?.map((item, index) => (
                <Grid
                  item
                  xs={12}
                  height="calc(30px + 30vmin)"
                  p={2}
                  className={styles.watchListCardContainer}
                >
                  <AnimCard
                    key={index}
                    data={item}
                    onClick={() => {
                      sessionStorage.setItem(
                        "animDetails",
                        JSON.stringify(item)
                      );
                      navigate("/animdetails");
                    }}
                  />
                  <Grid className={styles.watchListCardCover}>
                    <Tooltip title="delete" arrow placement="top">
                      <i
                        className="fas fa-trash"
                        style={{ cursor: "pointer" }}
                        onClick={() => deleteWatchList(item)}
                      />
                    </Tooltip>
                  </Grid>
                </Grid>
              ))}
            </Grid>
          </Grid>
        )}
      </Grid>
      <Grid container className={styles.footer}>
        <Pagination
          page={pageNo}
          count={pagination.last_visible_page}
          sx={{
            "& .css-yuzg60-MuiButtonBase-root-MuiPaginationItem-root": {
              height: "calc(4px + 4vmin)",
              minWidth: "calc(4px + 4vmin)",
            },
          }}
          onChange={(e, page) => setPageNo(page)}
        />
      </Grid>
      <Menu
        open={openFilter}
        anchorEl={filterMenuEl}
        onClose={() => setfilterMenuEl(null)}
      >
        {filterTypes.map((type, index) => (
          <MenuItem key={index} className={styles.filterMenuItem}>
            <label htmlFor={type} style={{ width: "100%" }}>
              <Grid container>
                <Grid item xs={2}>
                  <input
                    id={type}
                    type="checkbox"
                    checked={filterItems?.includes(type)}
                    onChange={(e) => {
                      if (e.target.checked) {
                        const filterItemsCopy = [...filterItems];
                        filterItemsCopy.push(type);
                        setFilterItems(filterItemsCopy);
                      } else {
                        const filterItemsCopy = [...filterItems];
                        const index = filterItemsCopy.findIndex(
                          (item) => item === type
                        );
                        filterItemsCopy.splice(index, 1);
                        setFilterItems(filterItemsCopy);
                      }
                    }}
                  />
                </Grid>
                <Grid item xs={10}>
                  {type}
                </Grid>
              </Grid>
            </label>
          </MenuItem>
        ))}
      </Menu>
      <div id="loader" style={{ display: "none" }}>
        <CircularProgress className={styles.spinner} />
      </div>
    </Grid>
  );
}
