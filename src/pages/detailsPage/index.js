import { Grid, IconButton, Tooltip } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { formatDate } from "../../data/getFormattedDate";
import styles from "../../styles/DetailsPage.module.css";

export default function DetailsPage() {
  const [animData, setAnimData] = useState({});

  const navigate = useNavigate();

  useEffect(() => {
    setAnimData(JSON.parse(sessionStorage.getItem("animDetails")));
  }, []);

  return (
    <Grid container className={styles.detailsPageContainer}>
      <Grid item xs={12} p={1}>
        <Grid container>
          <Grid item xs={2}>
            <Tooltip title="Back" arrow>
              <IconButton
                className={styles.iconButton}
                onClick={() => navigate(-1)}
              >
                <i className="fas fa-arrow-left" />
              </IconButton>
            </Tooltip>
          </Grid>
          <Grid item xs={8} className={styles.detailsTitle}>
            {animData?.title}
          </Grid>
          <Grid item xs={2}></Grid>
        </Grid>
        <Grid container className={styles.detailsTitle} pt={3} pb={2}>
          <iframe
            title={animData?.title}
            src={animData?.trailer?.embed_url}
            className={styles.animVideoDisplay}
          />
        </Grid>
        <Grid container pt={3}>
          <Grid item xs={2} className={styles.detailsLabel}>
            Total episodes
          </Grid>
          <Grid item xs={10} className={styles.details}>
            : {animData?.episodes}
          </Grid>
          <Grid item xs={2} className={styles.detailsLabel}>
            Aired date
          </Grid>
          <Grid item xs={10} className={styles.details}>
            : {formatDate(animData?.aired?.from)} to{" "}
            {animData?.aired?.to ? formatDate(animData?.aired?.to) : "Present"}
          </Grid>
          <Grid item xs={2} className={styles.detailsLabel}>
            Content type
          </Grid>
          <Grid item xs={10} className={styles.details}>
            : {animData?.genres?.map((genre) => genre.name)?.join(", ")}
          </Grid>
          <Grid item xs={12} className={styles.detailsLabel}>
            Synopsis :
          </Grid>
          <Grid item xs={12} p={1} className={styles.details}>
            {animData?.synopsis}
          </Grid>
          <Grid item xs={12} className={styles.detailsLabel}>
            Background :
          </Grid>
          <Grid item xs={12} p={1} className={styles.details}>
            {animData?.background}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
