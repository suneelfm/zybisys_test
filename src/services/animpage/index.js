import serviceUtil from "../utils";

export const getAnimData = async (pageNo) => {
  return serviceUtil
    .get(`/anime?page=${pageNo}`)
    .then((res) => ({ data: res.data }))
    .catch((error) => ({ error }));
};
