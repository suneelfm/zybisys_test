import axiosInstance from "../axiosConfig";

const get = (url) => {
  return axiosInstance.get(url);
};

const getWithResq = (url, reqObj) => {
  return axiosInstance.get(url, reqObj);
};

const post = (url, reqObj) => {
  return axiosInstance.post(url, reqObj);
};

const put = (url, reqObj) => {
  return axiosInstance.put(url, reqObj);
};

const remove = (url, id) => {
  return axiosInstance.delete(`${url}/${id}`);
};

const deleteById = (url, id) => {
  return axiosInstance.delete(`${url}/${id}`);
};

const deleteAll = (url, reqObj) => {
  return axiosInstance.delete(url, { data: reqObj });
};

const serviceUtil = {
  get,
  post,
  put,
  remove,
  deleteById,
  deleteAll,
  getWithResq,
};

export default serviceUtil;
