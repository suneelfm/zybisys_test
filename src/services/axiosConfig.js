import axios from "axios";

export const baseUrl = "https://api.jikan.moe/v4/";

axios.defaults.baseURL = baseUrl;
const axiosInstance = axios.create({
  baseUrl,
});

axiosInstance.interceptors.request.use(
  function (config) {
    document.getElementById("loader")?.classList.add("loaderContainer");
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  function (response) {
    setTimeout(() => {
      document.getElementById("loader")?.classList.remove("loaderContainer");
    }, 2000);
    return response;
  },
  function (error) {
    setTimeout(() => {
      document.getElementById("loader")?.classList.remove("loaderContainer");
    }, 2000);
    return Promise.reject(error);
  }
);

export default axiosInstance;
